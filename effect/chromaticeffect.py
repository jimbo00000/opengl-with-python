# chromaticeffect.py

from OpenGL.GL import *
import numpy
from util.shaderwithvariables import *
from util.fbo import *
from onepasseffect import *

crshader_vert_src = '''
#version 410 core

in vec4 vPosition;
out vec3 vfTexCoord;

void main()
{
    vfTexCoord = vec3(.5*(vPosition.xy+1.),0.);
    gl_Position = vec4(vPosition.xy, 0., 1.);
}
'''

crshader_frag_src = '''
#version 330

in vec3 vfTexCoord;
out vec4 fragColor;
uniform sampler2D tex;
uniform vec3 colorphase;

void main()
{
    float a = .01;
    vec2 off_r = a*vec2(sin(colorphase.r), cos(colorphase.r));
    vec2 off_g = a*vec2(sin(colorphase.g), cos(colorphase.g));
    vec2 off_b = a*vec2(sin(colorphase.b), cos(colorphase.b));

    float colr = texture(tex, vfTexCoord.xy + off_r).r;
    float colg = texture(tex, vfTexCoord.xy + off_g).g;
    float colb = texture(tex, vfTexCoord.xy + off_b).b;

    fragColor = vec4(colr, colg, colb, 1.);
}
'''

class ChromaticEffect(OnePassEffect):
    def __init__(self, w, h):
        super(ChromaticEffect, self).__init__(w, h)
        self.colorphase = [0,0,0]

    def getShaderSources(self):
        return crshader_vert_src, crshader_frag_src

    def Present(self, win_w, win_h):
        """Cover the viewport in pixels."""
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        glUseProgram(self.shader.program)

        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.fbo.texId)
        glUniform1i(self.shader.unis['tex'], 0)

        glUniform3f(self.shader.unis['colorphase'], self.colorphase[0], self.colorphase[1], self.colorphase[2])

        glBindVertexArray(self.shader.vao)
        glDrawElements(GL_TRIANGLES, 3*2, GL_UNSIGNED_INT, None)
        glBindVertexArray(0)

        glUseProgram(0)
        
    def timestep(self, absTime, dt):
        self.colorphase[0] = 10.0 * absTime
        self.colorphase[1] = 10.2 * absTime
        self.colorphase[2] = 10.3 * absTime
