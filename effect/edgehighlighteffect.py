# edgehighlighteffect.py

from OpenGL.GL import *
import numpy
from util.shaderwithvariables import *
from util.fbo import *
from onepasseffect import *

edgeshader_vert_src = '''
#version 410 core

in vec4 vPosition;
out vec3 vfTexCoord;

void main()
{
    vfTexCoord = vec3(.5*(vPosition.xy+1.),0.);
    gl_Position = vec4(vPosition.xy, 0., 1.);
}
'''

edgeshader_frag_src = '''
#version 330

in vec3 vfTexCoord;
out vec4 fragColor;
uniform sampler2D tex;

uniform int ResolutionX;
uniform int ResolutionY;

// Simple one-pass blurring
#define KERNEL_SIZE 9
float step_x = 1./float(ResolutionX);
float step_y = 1./float(ResolutionY);

vec2 offset[KERNEL_SIZE] = vec2[](
    vec2(-step_x, -step_y), vec2(0.0, -step_y), vec2(step_x, -step_y),
    vec2(-step_x,     0.0), vec2(0.0,     0.0), vec2(step_x,     0.0),
    vec2(-step_x,  step_y), vec2(0.0,  step_y), vec2(step_x,  step_y)
);

float kernel[KERNEL_SIZE] = float[](
#if 0
    1./16., 2./16., 1./16.,
    2./16., 4./16., 2./16.,
    1./16., 2./16., 1./16.

    0., 1., 0.,
    1., -4., 1.,
    0., 1., 0.
#else
    1., 2., 1.,
    0., 0., 0.,
    -1., -2., -1.
#endif
);

void main()
{
    vec4 sum = vec4(0.);
    int i;
    for( i=0; i<KERNEL_SIZE; i++ )
    {
        vec4 tc = texture(tex, vfTexCoord.xy + offset[i]);
        sum += tc * kernel[i];
    }
    if (sum.x + sum.y + sum.z > .1)
        sum = vec4(vec3(1.)-sum.xyz,1.);
    fragColor = sum;
}
'''

class EdgeHighlightEffect(OnePassEffect):
    def __init__(self, w, h):
        super(EdgeHighlightEffect, self).__init__(w, h)

    def getShaderSources(self):
        return edgeshader_vert_src, edgeshader_frag_src

    def Present(self, win_w, win_h):
        """Cover the viewport in pixels."""
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        glUseProgram(self.shader.program)

        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.fbo.texId)
        glUniform1i(self.shader.unis['tex'], 0)

        glUniform1i(self.shader.unis['ResolutionX'], win_w)
        glUniform1i(self.shader.unis['ResolutionY'], win_h)

        glBindVertexArray(self.shader.vao)
        glDrawElements(GL_TRIANGLES, 3*2, GL_UNSIGNED_INT, None)
        glBindVertexArray(0)

        glUseProgram(0)
