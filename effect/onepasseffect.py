# onepasseffect.py

from OpenGL.GL import *
import numpy
from util.shaderwithvariables import *
from util.fbo import *

fxshader_vert_src = '''
#version 410 core

in vec4 vPosition;
out vec3 vfTexCoord;

void main()
{
    vfTexCoord = vec3(.5*(vPosition.xy+1.),0.);
    gl_Position = vec4(vPosition.xy, 0., 1.);
}
'''

fxshader_frag_src = '''
#version 330

in vec3 vfTexCoord;
out vec4 fragColor;
uniform sampler2D tex;

void main()
{
    vec2 tc = vfTexCoord.xy;
    tc.y = 1. - tc.y; // just flip it over
    vec4 col = texture(tex, tc);
    fragColor = col;
}
'''

class OnePassEffect(object):
    def __init__(self, w, h):
        self.shader = ShaderWithVariables()
        self.fbo = FBO(w, h)

    def getShaderSources(self):
        return fxshader_vert_src, fxshader_frag_src

    def initGL(self):
        """Shaders must be initialized after a GL context is available."""
        if not glUseProgram:
            print 'Missing Shader Objects!'
            return
        srcs = self.getShaderSources()
        self.shader.initGL_program(srcs[0], srcs[1])

        self.shader.vao = glGenVertexArrays(1)
        glBindVertexArray(self.shader.vao)
        self._InitQuadAttributes()
        glBindVertexArray(0)

    def exitGL(self):
        pass # dtors should be called

    def _InitQuadAttributes(self):
        """Create VBOs for a fullscreen quad."""
        verts = [
            -1,-1,
            1,-1,
            1,1,
            -1,1,
        ]
        vertex_data = numpy.array(verts, numpy.float32)

        self.shader.vbos['vPosition'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.shader.vbos['vPosition'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW)
        # Note: the last parameter has to be None, 0 won't work!
        glVertexAttribPointer(self.shader.attrs['vPosition'], 2, GL_FLOAT, GL_FALSE, 0, None)
        
        faces = [
            0,1,2,
            0,2,3,
        ]
        glEnableVertexAttribArray(self.shader.attrs['vPosition'])

        quad_data = numpy.array(faces, numpy.uint32)
        
        self.shader.vbos['elements'] = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.shader.vbos['elements'])
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(faces)*sizeof(GLuint), quad_data, GL_STATIC_DRAW)

    def BindFBO(self):
        self.fbo.bind()

    def UnbindFBO(self):
        self.fbo.unbind()

    def ResizeFBO(self, w, h):
        del self.fbo
        self.fbo = FBO(w, h)

    def Present(self, win_w, win_h):
        """Cover the viewport in pixels."""
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        glUseProgram(self.shader.program)

        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.fbo.texId)
        glUniform1i(self.shader.unis['tex'], 0)

        glBindVertexArray(self.shader.vao)
        glDrawElements(GL_TRIANGLES, 3*2, GL_UNSIGNED_INT, None)
        glBindVertexArray(0)

        glUseProgram(0)
        
    def timestep(self, absTime, dt):
        pass

    def keyPressed(self, k):
        print("Effect Key press: ",k)
