#! /usr/bin/env python
# MatrixMath helper functions for OpenGL
# Original in C++ at https://github.com/jimbo00000/OpenGLMultiSkeleton

import math
import copy

#
# Vector Math
#


def length(v):
    return math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2])


def length2(v):
    return v[0]*v[0] + v[1]*v[1] + v[2]*v[2]


def normalize(v):
    n = math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2])
    vnorm = v
    vnorm[0] = v[0]/n
    vnorm[1] = v[1]/n
    vnorm[2] = v[2]/n
    return vnorm


def dot(a, b):
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]


def cross(b, c):
    a = [
        b[1]*c[2] - b[2]*c[1],
        b[2]*c[0] - b[0]*c[2],
        b[0]*c[1] - b[1]*c[0]]
    #normalize(a)
    return a


# TODO: vector class with operators
def add(a, b):
    d = [b[0]+a[0], b[1]+a[1], b[2]+a[2]]
    return d


def subtract(a, b):
    d = [b[0]-a[0], b[1]-a[1], b[2]-a[2]]
    return d


def multiply(a, s):
    p = [a[0]*s, a[1]*s, a[2]*s]
    return p

def transform(pt, mtx):
    """Transform a 3-space point by a 4x4 matrix.
    """
    vec = [
        mtx[0]*pt[0] + mtx[4]*pt[1] + mtx[8]*pt[2] + mtx[12],
        mtx[1]*pt[0] + mtx[5]*pt[1] + mtx[9]*pt[2] + mtx[13],
        mtx[2]*pt[0] + mtx[6]*pt[1] + mtx[10]*pt[2] + mtx[14],
        mtx[3]*pt[0] + mtx[7]*pt[1] + mtx[11]*pt[2] + mtx[15],
    ]
    vec3 = [
        vec[0] / vec[3],
        vec[1] / vec[3],
        vec[2] / vec[3],
    ]
    return vec3


#
# Matrix Math
#

def make_identity_matrix():
    """
    :rtype : 4x4 identity matrix as list
    """
    return [1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0]


def transposed(m):
    return [
        m[0], m[4], m[8], m[12],
        m[1], m[5], m[9], m[13],
        m[2], m[6], m[10], m[14],
        m[3], m[7], m[11], m[15]
    ]


def make_translation_matrix(x, y, z):
    mtx = make_identity_matrix()
    mtx[12] = x
    mtx[13] = y
    mtx[14] = z
    return mtx


def make_rotation_matrix(theta, axis):
    if not len(axis) == 3:
        return None
    #axis = normalize(axis)

    c = math.cos(theta)
    s = math.sin(theta)
    t = 1.0 - c
    x = axis[0]
    y = axis[1]
    z = axis[2]

    mtx = make_identity_matrix()
    mtx[0] = t*x*x + c
    mtx[1] = t*x*y - s*z
    mtx[2] = t*x*z + s*y
    mtx[3] = 0.0

    mtx[4] = t*x*y + s*z
    mtx[5] = t*y*y + c
    mtx[6] = t*y*z - s*x
    mtx[7] = 0.0

    mtx[8] = t*x*z - s*y
    mtx[9] = t*y*z + s*x
    mtx[10] = t*z*z + c
    mtx[11] = 0.0

    mtx[12] = 0.0
    mtx[13] = 0.0
    mtx[14] = 0.0
    mtx[15] = 1.0
    return mtx


def post_multiply(a, b):
    result = a[:]

    result[ 0] = b[ 0]*a[ 0] + b[ 1]*a[ 4] + b[ 2]*a[ 8] + b[ 3]*a[12]
    result[ 1] = b[ 0]*a[ 1] + b[ 1]*a[ 5] + b[ 2]*a[ 9] + b[ 3]*a[13]
    result[ 2] = b[ 0]*a[ 2] + b[ 1]*a[ 6] + b[ 2]*a[10] + b[ 3]*a[14]
    result[ 3] = b[ 0]*a[ 3] + b[ 1]*a[ 7] + b[ 2]*a[11] + b[ 3]*a[15]

    result[ 4] = b[ 4]*a[ 0] + b[ 5]*a[ 4] + b[ 6]*a[ 8] + b[ 7]*a[12]
    result[ 5] = b[ 4]*a[ 1] + b[ 5]*a[ 5] + b[ 6]*a[ 9] + b[ 7]*a[13]
    result[ 6] = b[ 4]*a[ 2] + b[ 5]*a[ 6] + b[ 6]*a[10] + b[ 7]*a[14]
    result[ 7] = b[ 4]*a[ 3] + b[ 5]*a[ 7] + b[ 6]*a[11] + b[ 7]*a[15]

    result[ 8] = b[ 8]*a[ 0] + b[ 9]*a[ 4] + b[10]*a[ 8] + b[11]*a[12]
    result[ 9] = b[ 8]*a[ 1] + b[ 9]*a[ 5] + b[10]*a[ 9] + b[11]*a[13]
    result[10] = b[ 8]*a[ 2] + b[ 9]*a[ 6] + b[10]*a[10] + b[11]*a[14]
    result[11] = b[ 8]*a[ 3] + b[ 9]*a[ 7] + b[10]*a[11] + b[11]*a[15]

    result[12] = b[12]*a[ 0] + b[13]*a[ 4] + b[14]*a[ 8] + b[15]*a[12]
    result[13] = b[12]*a[ 1] + b[13]*a[ 5] + b[14]*a[ 9] + b[15]*a[13]
    result[14] = b[12]*a[ 2] + b[13]*a[ 6] + b[14]*a[10] + b[15]*a[14]
    result[15] = b[12]*a[ 3] + b[13]*a[ 7] + b[14]*a[11] + b[15]*a[15]

    return result


def glh_translate(mtx, x, y, z):
    """
    :rtype : 4x4 matrix as list
    """
    if False:
        txm = make_translation_matrix(x, y, z)
        return post_multiply(mtx, txm)
    else: # Quicker version
        result = mtx[:]
        result[12] += mtx[0]*x + mtx[4]*y + mtx[8]*z
        result[13] += mtx[1]*x + mtx[5]*y + mtx[9]*z
        result[14] += mtx[2]*x + mtx[6]*y + mtx[10]*z
        return result


def glh_rotate(mtx, theta, axis):
    rotm = make_rotation_matrix(-theta * math.pi / 180.0, axis)
    return post_multiply(mtx, rotm)


def glh_rotatey(mtx, theta):
    t = -theta * math.pi / 180.0
    rotm = [
        math.cos(t), 0, math.sin(t), 0,
        0, 1, 0, 0,
        -math.sin(t), 0, math.cos(t), 0,
        0, 0, 0, 1,
    ]
    return post_multiply(mtx, rotm)


def glh_scale(mtx, scx, scy, scz):
    scmtx = [
        scx, 0.0, 0.0, 0.0,
        0.0, scy, 0.0, 0.0,
        0.0, 0.0, scz, 0.0,
        0.0, 0.0, 0.0, 1.0]
    return post_multiply(mtx, scmtx)


# Support for glhPerspectivef2, which is a standin for gluPerspective.
# http://www.opengl.org/wiki/GluPerspective_code
def glh_frustum(mtx, left, right, bottom, top, znear, zfar):
    temp = 2.0 * znear
    temp2 = right - left
    temp3 = top - bottom
    temp4 = zfar - znear

    mtx[0] = temp / temp2
    mtx[1] = 0.0
    mtx[2] = 0.0
    mtx[3] = 0.0
    mtx[4] = 0.0
    mtx[5] = temp / temp3
    mtx[6] = 0.0
    mtx[7] = 0.0
    mtx[8] = (right + left) / temp2
    mtx[9] = (top + bottom) / temp3
    mtx[10] = (-zfar - znear) / temp4
    mtx[11] = -1.0
    mtx[12] = 0.0
    mtx[13] = 0.0
    mtx[14] = (-temp * zfar) / temp4
    mtx[15] = 0.0
    return mtx


def glh_perspective(mtx, fovy_degrees, aspect_ratio, znear, zfar):
    ymax = znear * math.tan(fovy_degrees * math.pi / 360.0)
    #ymin = -ymax
    #xmin = -ymax * aspect_ratio
    xmax = ymax * aspect_ratio
    mtx = glh_frustum(mtx, -xmax, xmax, -ymax, ymax, znear, zfar)
    return mtx


def perspective_rh(yfov, aspect, znear, zfar):
    persp = make_identity_matrix()
    tan_half_fov = math.tan(yfov * 0.5)

    persp[0] = 1.0 / (aspect * tan_half_fov)
    persp[5] = 1.0 / tan_half_fov
    persp[10] = zfar / (znear - zfar)
    persp[11] = -1.0
    persp[14] = (zfar * znear) / (znear - zfar)
    persp[15] = 0.0
    return persp


def glh_look_at(mtx, eye, center, up):
    eyedir = list(center)  # deep copy
    eyedir[0] -= eye[0]
    eyedir[1] -= eye[1]
    eyedir[2] -= eye[2]
    fwd = normalize(eyedir)
    right = normalize(cross(fwd, up))
    up2 = cross(right, fwd)

    mtx[0] = right[0]
    mtx[4] = right[1]
    mtx[8] = right[2]

    mtx[1] = up2[0]
    mtx[5] = up2[1]
    mtx[9] = up2[2]

    mtx[2] = -fwd[0]
    mtx[6] = -fwd[1]
    mtx[10] = -fwd[2]

    mtx[15] = 1.0

    mtx = glh_translate(mtx, -eye[0], -eye[1], -eye[2])
    return mtx


def matrix_from_quaternion(mtx, quat):
    w = quat[0]
    x = quat[1]
    y = quat[2]
    z = quat[3]
    mtx[0] = 1.0 - 2.0 * (y * y + z * z)
    mtx[1] = 2.0 * (x * y + z * w)
    mtx[2] = 2.0 * (x * z - y * w)
    mtx[3] = 0.0
    mtx[4] = 2.0 * (x * y - z * w)
    mtx[5] = 1.0 - 2.0 * (x * x + z * z)
    mtx[6] = 2.0 * (z * y + x * w)
    mtx[7] = 0.0
    mtx[8] = 2.0 * (x * z + y * w)
    mtx[9] = 2.0 * (y * z - x * w)
    mtx[10] = 1.0 - 2.0 * (x * x + y * y)
    mtx[11] = 0.0
    mtx[12] = 0.0
    mtx[13] = 0.0
    mtx[14] = 0.0
    mtx[15] = 1.0
    return mtx


def glh_ortho(leftVal,
              rightVal,
              bottomVal,
              topVal,
              nearVal,
              farVal):
    A  =  2 / (rightVal -   leftVal)
    B  =  2 / (  topVal - bottomVal)
    C  = -2 / (  farVal -   nearVal)
    tx = -(rightVal +   leftVal) / (rightVal -   leftVal)
    ty = -(topVal   + bottomVal) / (topVal   - bottomVal)
    tz = -(farVal   +   nearVal) / (farVal   -   nearVal)

    mtx = [
        A, 0, 0, 0,
        0, B, 0, 0,
        0, 0, C, 0,
        tx, ty, tz, 1,
    ]
    return mtx
