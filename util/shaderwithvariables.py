#! /usr/bin/env python
# ShaderWithVariables class - holds state data in addition to the GL program.

from OpenGL.GL import *
from OpenGL.GL.shaders import *
import ctypes


class ShaderWithVariables():
    def __init__(self):
        self.program = 0
        self.attrs = {}
        self.unis = {}
        self.vao = -1
        self.vbos = {}

    def __del__(self):
        glDeleteProgram(self.program)

        # TODO replace with pythonic interface when available
        vid = ctypes.c_uint(self.vao)
        glDeleteVertexArrays(1, vid)

        for k, v in self.vbos.items():
            vid = ctypes.c_uint(v)
            glDeleteBuffers(1, vid)

    def initGL_program(self, vertsrc, fragsrc):
        """Some items must be initialized after a GL context is available.
        """
        if not glUseProgram:
            print 'Missing Shader Objects!'
            sys.exit(1)
        try:
            self.program = compileProgram(
                compileShader(vertsrc, GL_VERTEX_SHADER),
                compileShader(fragsrc, GL_FRAGMENT_SHADER),)
        except RuntimeError as e:
            self.program = 0
            print("")
            print("*  VS-FS SHADER ERROR")
            print("*    " + e[0][:e[0].find('\n')])
            print("*    " + str(e[2]))
            print("")
        self.find_variables(vertsrc)
        self.find_variables(fragsrc)

    def initgeom_program(self, vertsrc, geomsrc, fragsrc):
        """Some items must be initialized after a GL context is available.
        """
        if not glUseProgram:
            print 'Missing Shader Objects!'
            sys.exit(1)
        try:
            self.program = compileProgram(
                compileShader(vertsrc, GL_VERTEX_SHADER),
                compileShader(geomsrc, GL_GEOMETRY_SHADER),
                compileShader(fragsrc, GL_FRAGMENT_SHADER),)
        except RuntimeError as e:
            self.program = 0
            print("")
            print("*  VS-GS-FS SHADER ERROR")
            print("*   " + e[0][:e[0].find('\n')])
            print("*   " + str(e[2]))
            print("")
        self.find_variables(vertsrc)
        self.find_variables(geomsrc)
        self.find_variables(fragsrc)

    def find_variables(self, src):
        """Note that this assumes we have one variable declaration per line.
        """
        for s in src.splitlines():
            words = s.strip().split()
            #print "[",words,"]"
            if len(words) == 3:
                var = words[2].strip(';')
                #print "VAR:", words[0], var
                if words[0] == "uniform":
                    #print "  UNIFORM", var
                    self.unis[var] = glGetUniformLocation(self.program, var)
                elif words[0] == "in":
                    #print "  ATTR", var
                    self.attrs[var] = glGetAttribLocation(self.program, var)
