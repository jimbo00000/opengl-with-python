#! /usr/bin/env python
# FPS Timer in python
# Original in C++ at https://github.com/jimbo00000/OpenGLMultiSkeleton

from collections import deque
import time


class FPSTimer(object):
    """A sliding window of frame time measurements for maintaining a count
    of average frame rate.
    """
    def __init__(self):
        self.d = deque([], 100)

    def on_frame(self):
        t = time.clock()
        self.d.append(t)

    def get_fps(self):
        if len(self.d) < 1:
            return 0.0
        dt = self.d[-1] - self.d[0]
        if dt <= 0.0:
            return 0
        fps = len(self.d) / dt
        return int(fps)
