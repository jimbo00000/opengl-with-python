# fbo.py

from OpenGL.GL import *
import ctypes

class FBO():
    def __init__(self, w, h):
        self.w, self.h = w, h
        self.fboId = 0
        self.depthId = 0
        self.texId = 0
        self.fboId = glGenFramebuffers(1)
        glBindFramebuffer(GL_FRAMEBUFFER, self.fboId)

        self.depthId = glGenTextures(1)
        glBindTexture(GL_TEXTURE_2D, self.depthId)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
                      w, h, 0,
                      GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, None)
        glBindTexture(GL_TEXTURE_2D, 0)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, self.depthId, 0)

        self.texId = glGenTextures(1)
        glBindTexture(GL_TEXTURE_2D, self.texId)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,
                      w, h, 0,
                      GL_RGBA, GL_UNSIGNED_BYTE, None)
        glBindTexture(GL_TEXTURE_2D, 0)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, self.texId, 0)

        status = glCheckFramebufferStatus(GL_FRAMEBUFFER)
        if status != GL_FRAMEBUFFER_COMPLETE:
            print("ERROR: Framebuffer status: ", status)

        glBindFramebuffer(GL_FRAMEBUFFER, 0)

    def __del__(self):
        glDeleteTextures(self.texId)
        glDeleteTextures(self.depthId)
        glDeleteFramebuffers(1, [self.fboId])

    def bind(self):
        glBindFramebuffer(GL_FRAMEBUFFER, self.fboId)

    def unbind(self):
        glBindFramebuffer(GL_FRAMEBUFFER, 0)
