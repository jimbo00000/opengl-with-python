# scene.py
# Base interface class

class Scene(object):
    def __init__(self):
        pass

    def initGL(self):
        pass

    def timestep(self, dt):
        pass

    def RenderForOneEye(self, mview, persp):
        """Pass in the matrices"""
        pass
