# scene02.py
# Vertex arrays
from OpenGL.GL import *
import ctypes
import math
import util.matrixmath as mm

class Scene02(object):
    def __init__(self,n):
        self.bounce = 0
        self.n = n

    def initGL(self):
        pass

    def timestep(self, absTime, dt):
        self.bounce += dt * 2 * (180 / math.pi)

    def RenderForOneEye(self, mview, persp):
        """Pass in the matrices"""
        #prmtx = numpy.array(persp, numpy.float32)
        SixteenFloats = ctypes.c_float * 16
        prmtx = SixteenFloats(*persp)
        glMatrixMode(GL_PROJECTION)
        glLoadMatrixf(prmtx)

        n = self.n
        for i in range(n):
            mv = mview
            phase = float(i) / float(n)
            mv = mm.glh_rotate(mv, 360*phase, [0,1,0])
            amp = .01
            rad = 3
            mv = mm.glh_translate(mv, 0, .2*math.sin(amp*self.bounce +2*math.pi*phase), rad)
            #mvmtx = numpy.array(mv, numpy.float32)
            SixteenFloats = ctypes.c_float * 16
            mvmtx = SixteenFloats(*mv)
            glMatrixMode(GL_MODELVIEW)
            glLoadMatrixf(mvmtx)
            self.drawCube()

    def drawCube(self):
        """Draw a color cube using vertex arrays."""
        cols = [
            0,0,0,
            1,0,0,
            1,1,0,
            0,1,0,
            0,0,1,
            1,0,1,
            1,1,1,
            0,1,1,
        ]
        
        verts = [
            0,0,0,
            1,0,0,
            1,1,0,
            0,1,0,
            0,0,1,
            1,0,1,
            1,1,1,
            0,1,1,
        ]

        faces = [
            0,3,2, 1,0,2,
            4,5,6, 7,4,6,
            1,2,6, 5,1,6,
            2,3,7, 6,2,7,
            3,0,4, 7,3,4,
            0,1,5, 4,0,5
        ]

        glEnable(GL_CULL_FACE)
        glEnableClientState(GL_VERTEX_ARRAY)
        glEnableClientState(GL_COLOR_ARRAY)
        
        #v = numpy.array(verts, numpy.float32)
        #c = numpy.array(cols, numpy.float32)
        NumVertsFloats = ctypes.c_float * len(verts)
        v = NumVertsFloats(*verts)
        c = NumVertsFloats(*cols)
        glVertexPointer(3, GL_FLOAT, 0, v)
        glColorPointer(3, GL_FLOAT, 0, c)

        #f = numpy.array(faces, numpy.uint32)
        NumFacesUints = ctypes.c_uint32 * len(faces)
        f = NumFacesUints(*faces)
        glDrawElements(GL_TRIANGLES, 6*3*2, GL_UNSIGNED_INT, f)

        glDisableClientState(GL_VERTEX_ARRAY)
        glDisableClientState(GL_COLOR_ARRAY)
