# snakebodyscene.py
# Fabricate the shape of a cylinder.

from OpenGL.GL import *
import numpy
import math
import util.matrixmath as mm
from util.shaderwithvariables import *

snakeskin_vert_src = '''
#version 330

in vec3 vPosition;
in vec3 vColor;

out vec3 vfColor;
flat out int flipSeg;

uniform mat4 mvmtx;
uniform mat4 prmtx;
uniform int instanceCount;
uniform float time;

// The smoothly tapering shape of a snake body - has a hump
// in the middle and flares out toward the head side.
float getRadiusScale(float x)
{
    float r1 = sin(2.2*(1-x)); // taper toward tail, thickest around 1/4 in
    float r2 = .85 / (1.+100.*pow(abs(x-.01),2.)); // thicken up the neck
    float ss = smoothstep(.1-.05, .1+.05, x);
    float r = r1*ss + r2*(1.-ss);
    return r;
}

// Slithering motion
vec2 getSlitheringDisplacement(float t, float z)
{
    return vec2(sin(t+z), cos(1.3*(t+z)));
}

void main()
{
    vfColor = vColor.xyz;
    flipSeg = gl_InstanceID % 2;

    vec3 pos = vPosition;

    // Apply tapering snake body shape
    float x = (float(gl_InstanceID) + pos.z)  / float(instanceCount);
    pos.xy *= getRadiusScale(x);

    // Flatten cylinder a bit to match head
    pos.x *= 1. + .35*(1.-smoothstep(0., .1, x));

    float zScale = 10. / float(instanceCount);
    pos.z *= zScale;
    pos.z += float(gl_InstanceID) * zScale;

    pos.xy += .05 * pos.z * getSlitheringDisplacement(-time, pos.z);

    gl_Position = prmtx * mvmtx * vec4(pos, 1.);
}
'''

snakeskin_frag_src = '''
#version 330

in vec3 vfColor;
flat in int flipSeg;
out vec4 fragColor;


// https://www.shadertoy.com/view/XtSSzD
#define PI 3.1415926
float round(float a) { return floor(a+0.5); }
float saturate(float a){return clamp(a,0.0,1.0);}

float aIsGreater(float A, float B)
{
   float diff = A-B;
   return 0.5+0.5*abs(diff)/(diff);
}

float sech(float a)
{
    a= abs(a);
    return saturate( 2.0/(exp(a) + 1.0/exp(a)) );
}
float tri(float a)
{    
    return abs(1.0-mod(2.0*a+1.0,2.0));
}

float stp(float a)
{
    return mod(a,2.0) - fract(a);
}

float dispRange = 0.7;
float dispStrength = 0.02;
float dispPow = 1.5;
float numHexWide = 10.0;

void getScalyColor( out vec4 fragColor, in vec2 uv )
{
    uv.y += .05; // try and get hexes to wrap on y
    vec2 diff = vec2(1.);
    float dist = length(diff);
    float disp = pow(sech(dist/dispRange), dispPow)*dispStrength;
    uv += normalize(diff)*disp;
    
    vec2 tuv = uv*numHexWide;
    tuv.x += 0.5*stp(uv.y*numHexWide*.5);
    
    vec2 tempuv = floor(tuv)/numHexWide;
    tempuv.y = floor(tuv.y*0.5)/numHexWide;
    
    
    float trimod = aIsGreater(fract(tuv.y), tri(tuv.x));
    vec2 triuv = vec2(tuv.x - (0.5 - stp(tuv.y*0.5+0.5 )) , tuv.y*0.5 + 0.5);
    triuv = floor(triuv)/numHexWide;
    
    tempuv = mix(tempuv, triuv , trimod*mod(floor(tuv.y), 2.0));
    fragColor = vec4(tempuv, 0., 1.);
}

void main()
{
    vec2 uv = vfColor.xy;
    if (flipSeg == 1)
        uv.y = 1.-uv.y;
    getScalyColor(fragColor, uv);
    //fragColor = vec4(vfColor.xyz, 1.);
}
'''

class SnakeBodyScene(object):
    def __init__(self, n):
        self.cubes = ShaderWithVariables()
        self.n = n
        self.subdiv = 16
        self.time = 0
        self.instanceCount = 16

    def initGL(self):
        """Shaders must be initialized after a GL context is available."""
        if not glUseProgram:
            print 'Missing Shader Objects!'
            return
        self.cubes.initGL_program(snakeskin_vert_src, snakeskin_frag_src)

        self.cubes.vao = glGenVertexArrays(1)
        glBindVertexArray(self.cubes.vao)
        self._InitArrowShapeAttributes()
        glBindVertexArray(0)

    def exitGL(self):
        del self.cubes

    def _InitArrowShapeAttributes(self):
        """Fabricate a mesh's attribute data."""
        n = self.subdiv

        def make_cylinder_mesh():
            """Returns the vertices, colors and face indices of a cone."""
            verts = []
            cols = []
            faces = []
            r = .3
            zlen = 1
            for i in range(n+1):
                phase = float(i)/float(n)
                rot = 2 * math.pi * (phase+.5)
                x,y = math.sin(rot), math.cos(rot)
                verts.extend([ [r*x,r*y,0], [r*x,r*y,zlen] ])
                cols.extend([ [phase,0,0], [phase,1,0] ])
            for i in range(n):
                faces.append([2*i  , 2*i+3, 2*i+2])
                faces.append([2*i  , 2*i+1, 2*i+3])
            return verts, cols, faces

        verts, cols, faces = make_cylinder_mesh()

        # Extend each 3-vector into a 4-vector with w=1
        verts = [v+[1] for v in verts]

        # Flatten lists of lists
        verts = [val for sublist in verts for val in sublist]
        cols = [val for sublist in cols for val in sublist]
        faces = [val for sublist in faces for val in sublist]
        self.num_verts = len(verts)

        vertex_data = numpy.array(verts, numpy.float32)
        color_data = numpy.array(cols, numpy.float32)

        self.cubes.vbos['vPosition'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.cubes.vbos['vPosition'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW)
        # Note: the last parameter has to be None, 0 won't work!
        glVertexAttribPointer(self.cubes.attrs['vPosition'], 4, GL_FLOAT, GL_FALSE, 0, None)
        
        self.cubes.vbos['vColor'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.cubes.vbos['vColor'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*sizeof(GLfloat), color_data, GL_STATIC_DRAW)
        glVertexAttribPointer(self.cubes.attrs['vColor'], 3, GL_FLOAT, GL_FALSE, 0, None)

        glEnableVertexAttribArray(self.cubes.attrs['vPosition'])
        glEnableVertexAttribArray(self.cubes.attrs['vColor'])

        quad_data = numpy.array(faces, numpy.uint32)
        
        self.cubes.vbos['elements'] = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.cubes.vbos['elements'])
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(faces)*sizeof(GLuint), quad_data, GL_STATIC_DRAW)

    def timestep(self, absTime, dt):
        self.time = absTime

    def drawSingleTube(self, mview, persp):
        """Pass in the matrices and draw a single arrow shape."""
        glDisable(GL_CULL_FACE)
        mview = mm.glh_scale(mview, 4,4,4)
        glUseProgram(self.cubes.program)
        glUniformMatrix4fv(self.cubes.unis['mvmtx'], 1, GL_FALSE, numpy.array(mview, numpy.float32))
        glUniformMatrix4fv(self.cubes.unis['prmtx'], 1, GL_FALSE, numpy.array(persp, numpy.float32))

        glBindVertexArray(self.cubes.vao)
        x = self.subdiv
        glDrawElements(GL_TRIANGLES, x*3*5, GL_UNSIGNED_INT, None)
        glBindVertexArray(0)

        glUseProgram(0)

    def drawInstancedTube(self, mview, persp):
        """Pass in the matrices and draw a series of instanced cylinders."""
        glEnable(GL_CULL_FACE)
        mview = mm.glh_translate(mview, 0, 0, -.01*self.instanceCount)
        mview = mm.glh_scale(mview, 4,4,4)
        glUseProgram(self.cubes.program)
        glUniformMatrix4fv(self.cubes.unis['mvmtx'], 1, GL_FALSE, numpy.array(mview, numpy.float32))
        glUniformMatrix4fv(self.cubes.unis['prmtx'], 1, GL_FALSE, numpy.array(persp, numpy.float32))
        glUniform1i(self.cubes.unis['instanceCount'], self.instanceCount)
        glUniform1f(self.cubes.unis['time'], self.time)

        glBindVertexArray(self.cubes.vao)
        x = self.subdiv
        glDrawElementsInstanced(GL_TRIANGLES, x*3*5, GL_UNSIGNED_INT, None, self.instanceCount)
        glBindVertexArray(0)

        glUseProgram(0)

    def RenderForOneEye(self, mview, persp):
        self.drawInstancedTube(mview, persp)
