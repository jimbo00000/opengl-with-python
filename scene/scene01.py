# scene01.py
# Fixed function pipeline, immediate mode
from OpenGL.GL import *
import ctypes
import math
import util.matrixmath as mm


class Scene01(object):
    def __init__(self,n):
        self.bounce = 0
        self.n = n

    def initGL(self):
        pass

    def timestep(self, absTime, dt):
        self.bounce += dt * 2 * (180 / math.pi)

    def RenderForOneEye(self, mview, persp):
        """Pass in the matrices"""
        # numpy is generally accepted as the best way to pass native float
        # arrays to the CFFI. However, it is a fairly large library and has
        # proven difficult to install in PyPy 4.0 on Windows.
        # Using ctypes for arrays allows this example to run on PyPy on my
        # machines, and to my surprise, I find it is much slower.
        #prmtx = numpy.array(persp, numpy.float32)
        SixteenFloats = ctypes.c_float * 16
        prmtx = SixteenFloats(*persp)
        glMatrixMode(GL_PROJECTION)
        glLoadMatrixf(prmtx)

        n = self.n
        for i in range(n):
            mv = mview
            phase = float(i) / float(n)
            mv = mm.glh_rotate(mv, 360*phase, [0,1,0])
            amp = .01
            rad = 3
            mv = mm.glh_translate(mv, 0, .2*math.sin(amp*self.bounce +2*math.pi*phase), rad)
            #mvmtx = numpy.array(mv, numpy.float32)
            mvmtx = SixteenFloats(*mv)
            glMatrixMode(GL_MODELVIEW)
            glLoadMatrixf(mvmtx)
            self.drawCube()

    def drawCube(self):
        """Draw a color cube in immediate mode"""
        glBegin(GL_QUADS)

        glColor3f(0,0,0)
        glVertex3f(0,0,0)
        glColor3f(1,0,0)
        glVertex3f(1,0,0)
        glColor3f(1,1,0)
        glVertex3f(1,1,0)
        glColor3f(0,1,0)
        glVertex3f(0,1,0)

        glColor3f(0,0,1)
        glVertex3f(0,0,1)
        glColor3f(1,0,1)
        glVertex3f(1,0,1)
        glColor3f(1,1,1)
        glVertex3f(1,1,1)
        glColor3f(0,1,1)
        glVertex3f(0,1,1)

        glColor3f(1,0,0)
        glVertex3f(1,0,0)
        glColor3f(1,0,1)
        glVertex3f(1,0,1)
        glColor3f(1,1,1)
        glVertex3f(1,1,1)
        glColor3f(1,1,0)
        glVertex3f(1,1,0)

        glColor3f(0,0,0)
        glVertex3f(0,0,0)
        glColor3f(0,0,1)
        glVertex3f(0,0,1)
        glColor3f(0,1,1)
        glVertex3f(0,1,1)
        glColor3f(0,1,0)
        glVertex3f(0,1,0)

        glColor3f(0,0,0)
        glVertex3f(0,0,0)
        glColor3f(0,0,1)
        glVertex3f(0,0,1)
        glColor3f(1,0,1)
        glVertex3f(1,0,1)
        glColor3f(1,0,0)
        glVertex3f(1,0,0)

        glColor3f(0,1,0)
        glVertex3f(0,1,0)
        glColor3f(0,1,1)
        glVertex3f(0,1,1)
        glColor3f(1,1,1)
        glVertex3f(1,1,1)
        glColor3f(1,1,0)
        glVertex3f(1,1,0)

        glEnd()
