# voronoifragshaderscene.py

from OpenGL.GL import *
import numpy
import math
import util.matrixmath as mm
from util.shaderwithvariables import *

fragshader_vert_src = '''
#version 330

in vec4 vPosition;
out vec3 vfColor;

void main()
{
    vfColor = vPosition.xyz;
    gl_Position = vPosition;
}
'''

fragshader_frag_src = '''
#version 330

in vec3 vfColor;
out vec4 fragColor;

// Shane's shadertoys are one of the web;s best resources.
// https://www.shadertoy.com/view/4lBSzW
float Voronoi3Tap(vec2 p)
{
    vec2 s = floor(p + (p.x+p.y)*0.3660254);
    p -= s - (s.x+s.y)*0.2113249;
    float i = step(0.0, p.x-p.y); 
    vec2 p1 = p - vec2(i, 1.0-i) + 0.2113249, p2 = p - 0.5773502; 
    float d = min(min(dot(p, p), dot(p1, p1)), dot(p2, p2))/0.425;
    return sqrt(d);
}

void main()
{
    fragColor = vec4(vec3(Voronoi3Tap(10.*vfColor.xy)), 1.);
}
'''

class VoronoiFragShaderScene(object):
    def __init__(self):
        self.shader = ShaderWithVariables()

    def initGL(self):
        """Shaders must be initialized after a GL context is available."""
        if not glUseProgram:
            print 'Missing Shader Objects!'
            return
        self.shader.initGL_program(fragshader_vert_src, fragshader_frag_src)

        self.shader.vao = glGenVertexArrays(1)
        glBindVertexArray(self.shader.vao)
        self._InitQuadAttributes()
        glBindVertexArray(0)

    def exitGL(self):
        del self.shader

    def _InitQuadAttributes(self):
        """Create VBOs for a fullscreen quad."""
        verts = [
            -1,-1,
            1,-1,
            1,1,
            -1,1,
        ]
        vertex_data = numpy.array(verts, numpy.float32)

        self.shader.vbos['vPosition'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.shader.vbos['vPosition'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW)
        # Note: the last parameter has to be None, 0 won't work!
        glVertexAttribPointer(self.shader.attrs['vPosition'], 2, GL_FLOAT, GL_FALSE, 0, None)
        
        faces = [
            0,1,2,
            0,2,3,
        ]
        glEnableVertexAttribArray(self.shader.attrs['vPosition'])

        quad_data = numpy.array(faces, numpy.uint32)
        
        self.shader.vbos['elements'] = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.shader.vbos['elements'])
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(faces)*sizeof(GLuint), quad_data, GL_STATIC_DRAW)

    def timestep(self, absTime, dt):
        pass

    def drawFullscreenQuad(self, mview, persp):
        """Cover the viewport in pixels."""
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        glUseProgram(self.shader.program)

        glBindVertexArray(self.shader.vao)
        glDrawElements(GL_TRIANGLES, 3*2, GL_UNSIGNED_INT, None)
        glBindVertexArray(0)

        glUseProgram(0)

    def RenderForOneEye(self, mview, persp):
        self.drawFullscreenQuad(mview, persp)
