# scene04.py
# VBOs
from OpenGL.GL import *
import numpy
import math
import util.matrixmath as mm
from util.shaderwithvariables import *

basic_vert_src = '''
#version 300 es

precision mediump float;


in vec3 vPosition;
in vec3 vColor;

out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vec4(vPosition, 1.);
}
'''

basic_frag_src = '''
#version 300 es

precision mediump float;


in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.);
}
'''

class Scene04(object):
    def __init__(self, n):
        self.bounce = 0
        self.cubes = ShaderWithVariables()
        self.n = n

    def initGL(self):
        print("initGL")
        """Shaders must be initialized after a GL context is available."""
        if not glUseProgram:
            print 'Missing Shader Objects!'
            return
        self.cubes.initGL_program(basic_vert_src, basic_frag_src)
        self._InitCubeAttributes()

    def _InitCubeAttributes(self):
        cols = [
            0,0,0,
            1,0,0,
            1,1,0,
            0,1,0,
            0,0,1,
            1,0,1,
            1,1,1,
            0,1,1,
        ]
        
        verts = [
            0,0,0,
            1,0,0,
            1,1,0,
            0,1,0,
            0,0,1,
            1,0,1,
            1,1,1,
            0,1,1,
        ]

        faces = [
            0,3,2, 1,0,2,
            4,5,6, 7,4,6,
            1,2,6, 5,1,6,
            2,3,7, 6,2,7,
            3,0,4, 7,3,4,
            0,1,5, 4,0,5
        ]

        vertex_data = numpy.array(verts, numpy.float32)

        self.cubes.vbos['vPosition'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.cubes.vbos['vPosition'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*3*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW)
        # Note: the last parameter has to be None, 0 won't work!
        glVertexAttribPointer(self.cubes.attrs['vPosition'], 3, GL_FLOAT, GL_FALSE, 0, None)
        
        self.cubes.vbos['vColor'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.cubes.vbos['vColor'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*3*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW)
        glVertexAttribPointer(self.cubes.attrs['vColor'], 3, GL_FLOAT, GL_FALSE, 0, None)

        glEnableVertexAttribArray(self.cubes.attrs['vPosition'])
        glEnableVertexAttribArray(self.cubes.attrs['vColor'])

        quad_data = numpy.array(faces, numpy.uint32)
        
        self.cubes.vbos['elements'] = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.cubes.vbos['elements'])
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(faces)*3*sizeof(GLuint), quad_data, GL_STATIC_DRAW)

    def timestep(self, absTime, dt):
        self.bounce += dt * 2 * (180 / math.pi)

    def RenderForOneEye(self, mview, persp):
        """Pass in the matrices"""
        glUseProgram(self.cubes.program)

        proj_array = numpy.array(persp, numpy.float32)
        glUniformMatrix4fv(self.cubes.unis['prmtx'], 1, GL_FALSE, proj_array)

        n = self.n
        for i in range(n):
            mv = mview
            phase = float(i) / float(n)
            #mv = mm.glh_rotate(mv, 360*phase, [0,1,0])
            mv = mm.glh_rotatey(mv, 360*phase)
            amp = .01
            rad = 3
            mv = mm.glh_translate(mv, 0, .2*math.sin(amp*self.bounce +2*math.pi*phase), rad)

            mv_array = numpy.array(mv, numpy.float32)
            glUniformMatrix4fv(self.cubes.unis['mvmtx'], 1, GL_FALSE, mv_array)

            self.drawCube()

        glUseProgram(0)


    def drawCube(self):
        """Draw a color cube using vertex arrays."""

        glEnable(GL_CULL_FACE)
        glBindBuffer(GL_ARRAY_BUFFER, self.cubes.vbos['vPosition'])
        glBindBuffer(GL_ARRAY_BUFFER, self.cubes.vbos['vColor'])
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.cubes.vbos['elements'])

        glEnableVertexAttribArray(self.cubes.attrs['vPosition'])
        glEnableVertexAttribArray(self.cubes.attrs['vColor'])

        glDrawElements(GL_TRIANGLES, 6*3*2, GL_UNSIGNED_INT, None)
