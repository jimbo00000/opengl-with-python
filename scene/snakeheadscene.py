# snakeheadscene.py

from OpenGL.GL import *
import numpy
import math
import util.matrixmath as mm
from util.shaderwithvariables import *
from vectorfieldscene import *



snakeskin_pattern_frag_src = '''
#version 330

in vec3 vfColor;
out vec4 fragColor;

// https://www.shadertoy.com/view/XtSSzD
#define PI 3.1415926
float round(float a) { return floor(a+0.5); }
float saturate(float a){return clamp(a,0.0,1.0);}

float aIsGreater(float A, float B)
{
   float diff = A-B;
   return 0.5+0.5*abs(diff)/(diff);
}

float sech(float a)
{
    a= abs(a);
    return saturate( 2.0/(exp(a) + 1.0/exp(a)) );
}
float tri(float a)
{    
    return abs(1.0-mod(2.0*a+1.0,2.0));
}

float stp(float a)
{
    return mod(a,2.0) - fract(a);
}

float dispRange = 0.7;
float dispStrength = 0.02;
float dispPow = 1.5;
float numHexWide = 15.0;

void getScalyColor( out vec4 fragColor, in vec2 uv )
{
    //uv.y += .05; // try and get hexes to wrap on y
    vec2 diff = vec2(1.);
    float dist = length(diff);
    float disp = pow(sech(dist/dispRange), dispPow)*dispStrength;
    uv += normalize(diff)*disp;
    
    vec2 tuv = uv*numHexWide;
    tuv.x += 0.5*stp(uv.y*numHexWide*.5);
    
    vec2 tempuv = floor(tuv)/numHexWide;
    tempuv.y = floor(tuv.y*0.5)/numHexWide;
    
    float trimod = aIsGreater(fract(tuv.y), tri(tuv.x));
    vec2 triuv = vec2(tuv.x - (0.5 - stp(tuv.y*0.5+0.5 )) , tuv.y*0.5 + 0.5);
    triuv = floor(triuv)/numHexWide;
    
    tempuv = mix(tempuv, triuv , trimod*mod(floor(tuv.y), 2.0));
    fragColor = vec4(tempuv, 0., 1.);
}

void main()
{
    vec2 cylSlice = normalize(vfColor.xy);
    float at = atan(cylSlice.x, cylSlice.y);
    vec2 uv = vec2(.2*at, 3.*.1*(vfColor.z+3.5));
    getScalyColor(fragColor, uv);
}
'''

snakehead_vert_src = '''
#version 330

in vec3 vPosition;
in vec3 vColor;

out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vPosition.xyz;
    gl_Position = prmtx * mvmtx * vec4(vPosition, 1.);
}
'''

snakehead_frag_src = '''
#version 330

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.);
}
'''

# Modify each of the mesh's vertices in the buffer by a function.
# Modeling with vector fields.
comp_src = '''
#version 430

#define THREADS_PER_BLOCK 256
layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
uniform float strength;

vec3 vectorField(in vec3 pos);

float smin( float a, float b, float k )
{
    float h = clamp( 0.5+0.5*(b-a)/k, 0.0, 1.0 );
    return mix( b, a, h ) - k*h*(1.0-h);
}

float sdSphere( vec3 p, float s )
{
    return length(p)-s;
}

float udRoundBox( vec3 p, vec3 b, float r )
{
  return length(max(abs(p)-b,0.0))-r;
}

float sdHead( vec3 p )
{
    p *= .2; // scale *up*
    vec3 peye1 = p + vec3(.1, 0., .1);
    vec3 peye2 = p + vec3(-.1,0.,.1);
    vec3 psnout = p + vec3(0., 0., .3);
    float r = .2;
    float deyes = min(sdSphere(peye1, r), sdSphere(peye2, r));

    vec3 pbrow1 = p + vec3(.14, -.12, .32);
    vec3 pbrow2 = p + vec3(-.14, -.12, .32);
    deyes = min(deyes, sdSphere(pbrow1, r*.125));
    deyes = min(deyes, sdSphere(pbrow2, r*.125));
    deyes = smin(deyes, udRoundBox(psnout, vec3(.035, .015, .28), .1), .3);
    return deyes;
}

// Try raymarching towards origin
vec3 raymarchPoint(vec3 p)
{
    vec3 ro = p;
    vec3 rd = -p;
    vec3 step = strength * rd;
    for (int i=0; i<100; ++i)
    {
        float d = sdHead(ro);
        ro += step * d;
    }
    return ro;
}

vec3 distortPoint(vec3 p)
{
    return strength * vectorField((2.*p.xyz+vec3(2.)));
}

void main()
{
    int index = int(gl_GlobalInvocationID);
    vec4 p = positions[index];
    //p.xyz += distortPoint(p.xyz);
    p.xyz = raymarchPoint(p.xyz);
    positions[index] = p;
}
'''

vectorfield_src = '''
vec3 vectorField(in vec3 pos)
{
    vec3 wc = pos - vec3(2.);
    float f = 1.;
    vec2 planeCenter = vec2(2.) + 1.*vec2(cos(f*wc.z), sin(f*wc.z));
    vec3 dp = vec3(wc.xy-planeCenter, 0.);

    float mag = 1. - smoothstep(0.,3.,length(dp));
    vec3 pull = -dp;
    pull.z = 1.;
    return mag * pull;
}
'''

# Snake eyes
# Re-use the subdivided cube mesh and displace verts into a sphere.
# Color spheres flat black with white specular highlight.
snakeeye_vert_src = '''
#version 330

in vec3 vPosition;
in vec3 vColor;

out vec3 vfColor;
out vec3 vfNormal;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vec3 sphCent = vec3(.5); // Mesh generated in [0,1]
    vec3 toCenter = normalize(vPosition.xyz - sphCent);
    vec3 sphPos = 1.5 * toCenter;

    vfColor = vColor.xyz;
    vfNormal = normalize(mat3(mvmtx) * toCenter);
    gl_Position = prmtx * mvmtx * vec4(sphPos.xyz, 1.);
}
'''

snakeeye_frag_src = '''
#version 330

in vec3 vfNormal;
in vec3 vfColor;
out vec4 fragColor;

float shininess = 250.;
void main()
{
    vec3 N = vfNormal;
    vec3 L = normalize(vec3(0., 1., 1.5)); // direction *to* light, up like the sun
    vec3 E = vec3(0.);

    vec3 spec = vec3(0.);
    float bright = max(dot(N,L), 0.);
    if (bright > 0.)
    {
        vec3 H = normalize(L + E);
        float specBr = max(dot(H,N), 0.);
        spec = vec3(1.) * pow(specBr, shininess);
    }

    fragColor = vec4(spec, 1.);
}
'''


def makeComputeShader(computesrc):
    prog = compileProgram(
        compileShader(computesrc, GL_COMPUTE_SHADER))
    return prog


class SnakeHeadScene(object):
    """Draws the geometry for the head of a snake with 2 black eyes. Geometry is formed
    by displacing a subdivided cube mesh by a vector field using a compute shader.
    Displacement is performed once in initGL.
    """
    def __init__(self, n):
        self.bounce = 0
        self.cubes = ShaderWithVariables()
        self.eyesShader = ShaderWithVariables()
        self.n = n
        self.subdiv = 64
        self.comp_prog = 0

    def initGL(self):
        print("initGL")
        """Shaders must be initialized after a GL context is available."""
        if not glUseProgram:
            print 'Missing Shader Objects!'
            return
        self.cubes.initGL_program(snakehead_vert_src, snakeskin_pattern_frag_src)
        self.cubes.vao = glGenVertexArrays(1)
        glBindVertexArray(self.cubes.vao)
        self._InitCubeAttributes(self.cubes)
        glBindVertexArray(0)

        self.eyesShader.initGL_program(snakeeye_vert_src, snakeeye_frag_src)
        self.eyesShader.vao = glGenVertexArrays(1)
        glBindVertexArray(self.eyesShader.vao)
        self._InitCubeAttributes(self.eyesShader)
        glBindVertexArray(0)

        self.switchToField(0)
        self.runComputeShader(1.)

    def exitGL(self):
        del self.cubes
        del self.eyesShader
        glDeleteProgram(self.comp_prog)

    def _InitCubeAttributes(self, shaderWV):
        """Fabricate a subdivided cube by dividing a single square face into a grid
        then replicating it 6 times using matrix transformation.
        Resulting cube in [0,1].
        """
        n = self.subdiv

        verts = []
        faces = []
        def append_faces(mtx, flipface=False):
            basevert = len(verts)
            coords = numpy.linspace(0,1,n+1)
            for d in coords:
                for c in coords:
                    pt = [c,d,0]
                    pt = mm.transform(pt, mtx)
                    verts.append(pt)
            for j in range(n):
                r = j*(n+1) + basevert
                for i in range(n):
                    f1 = [r+i, r+i+1, r+i+n+1]
                    f2 = [r+i+1, r+i+n+2, r+i+n+1]
                    if flipface:
                        f1 = [f1[0], f1[2], f1[1]]
                        f2 = [f2[0], f2[2], f2[1]]
                    faces.append(f1)
                    faces.append(f2)

        mtx = [
            1,0,0,0,
            0,1,0,0,
            0,0,1,0,
            0,0,0,1,
        ]
        mtx2 = [
            1,0,0,0,
            0,0,1,0,
            0,-1,0,0,
            0,0,0,1,
        ]
        mtx3 = [
            0,1,0,0,
            0,0,1,0,
            -1,0,0,0,
            0,0,0,1,
        ]
        mtx4 = [
            1,0,0,0,
            0,1,0,0,
            0,0,1,0,
            0,0,1,1,
        ]
        mtx5 = [
            1,0,0,0,
            0,0,1,0,
            0,-1,0,0,
            0,1,0,1,
        ]
        mtx6 = [
            0,1,0,0,
            0,0,1,0,
            -1,0,0,0,
            1,0,0,1,
        ]
        append_faces(mtx, True)
        append_faces(mtx2)
        append_faces(mtx3, True)
        append_faces(mtx4)
        append_faces(mtx5, True)
        append_faces(mtx6)

        # Center cube on origin
        centermtx = mm.make_identity_matrix()
        centermtx = mm.glh_scale(centermtx, 2, 2, 2)
        centermtx = mm.glh_translate(centermtx, -.5, -.5, -.5)
        verts = [mm.transform(v, centermtx) for v in verts]

        # We need to extend each 3-list in the list with the value of 1
        # TODO This is hardly the pythonic way to do it...
        xx = []
        for v in verts:
            xx.append([v[0], v[1], v[2], 1])
        sphverts = xx

        # Flatten lists of lists
        cols = [val for sublist in verts for val in sublist]
        verts = [val for sublist in sphverts for val in sublist]
        faces = [val for sublist in faces for val in sublist]
        self.num_verts = len(verts)

        vertex_data = numpy.array(verts, numpy.float32)
        color_data = numpy.array(cols, numpy.float32)

        shaderWV.vbos['vPosition'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, shaderWV.vbos['vPosition'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW)
        # Note: the last parameter has to be None, 0 won't work!
        glVertexAttribPointer(shaderWV.attrs['vPosition'], 4, GL_FLOAT, GL_FALSE, 0, None)
        glEnableVertexAttribArray(shaderWV.attrs['vPosition'])
        
        if shaderWV.attrs['vColor'] > -1:
            shaderWV.vbos['vColor'] = glGenBuffers(1)
            glBindBuffer(GL_ARRAY_BUFFER, shaderWV.vbos['vColor'])
            glBufferData(GL_ARRAY_BUFFER, len(cols)*sizeof(GLfloat), color_data, GL_STATIC_DRAW)
            glVertexAttribPointer(shaderWV.attrs['vColor'], 3, GL_FLOAT, GL_FALSE, 0, None)
            glEnableVertexAttribArray(shaderWV.attrs['vColor'])

        quad_data = numpy.array(faces, numpy.uint32)
        
        shaderWV.vbos['elements'] = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shaderWV.vbos['elements'])
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(faces)*sizeof(GLuint), quad_data, GL_STATIC_DRAW)

    def switchToField(self, x):
        """Compile a new set of shaders with the given vector field index."""
        vectorfield_src = vectorfield_srcs[x]
        glDeleteProgram(self.comp_prog)
        self.comp_prog = makeComputeShader(comp_src + vectorfield_src)
        self.comp_unis = {}
        self.comp_unis['strength'] = glGetUniformLocation(self.comp_prog, 'strength')

    def runComputeShader(self, strength):
        """Displaces the vertices in cubes' VBO by a vector field with scalar magnitude.
        """
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, self.cubes.vbos['vPosition'])
        glUseProgram(self.comp_prog)
        glUniform1f(self.comp_unis['strength'], strength)
        glDispatchCompute(self.num_verts/256, 1, 1)
        glUseProgram(0)

    def timestep(self, absTime, dt):
        self.bounce += dt * 2 * (180 / math.pi)

    def drawSingleCube(self, mview, persp, swv):
        """The last parameter is the ShaderWithVariables object that holds a
        reference to itsd uiniform variable so we can re-use this function
        for both the head and eyeballs.
        """
        mv_array = numpy.array(mview, numpy.float32)
        glUniformMatrix4fv(swv.unis['mvmtx'], 1, GL_FALSE, mv_array)
        x = self.subdiv
        glDrawElements(GL_TRIANGLES, x*x*6*3*2, GL_UNSIGNED_INT, None)

    def RenderForOneEye(self, mview, persp):
        """Pass in the view and projection matrices from main."""
        glBindVertexArray(self.cubes.vao)
        glUseProgram(self.cubes.program)

        proj_array = numpy.array(persp, numpy.float32)
        glUniformMatrix4fv(self.cubes.unis['prmtx'], 1, GL_FALSE, proj_array)

        glEnable(GL_CULL_FACE)
        self.drawSingleCube(mview, persp, self.cubes)

        # Draw eyes
        for xoff in [-1,1]:
            glBindVertexArray(self.eyesShader.vao)
            glUseProgram(self.eyesShader.program)
            mv = mview
            mv = mm.glh_translate(mv, .88*xoff, .5, -1.8)
            s = .2
            mv = mm.glh_scale(mv, s, s, s)
            glUniformMatrix4fv(self.eyesShader.unis['prmtx'], 1, GL_FALSE, proj_array)
            self.drawSingleCube(mv, persp, self.eyesShader)

        glUseProgram(0)
        glBindVertexArray(0)
