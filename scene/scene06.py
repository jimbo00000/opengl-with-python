# scene06.py
# Instanced Rendering
from OpenGL.GL import *
import numpy
import math
import util.matrixmath as mm
from util.shaderwithvariables import *

basic_vert_src = '''
#version 330

in vec3 vPosition;
in vec3 vColor;

out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

uniform int u_numCubes;
uniform float u_bounce;

vec3 RotateY( const in vec3 vPos, const in float fAngle )
{ // https://www.shadertoy.com/view/lsXGzH
    float s = sin(fAngle);
    float c = cos(fAngle);
    vec3 vResult = vec3( c * vPos.x + s * vPos.z, vPos.y, -s * vPos.x + c * vPos.z);
    return vResult;
}

float pi = 3.1415927;
void main()
{
    // Note this is not exactly the same as Scene5.
    float phase = float(gl_InstanceID) / float(u_numCubes);
    float t = 2 * pi * phase;
    float amp = .01;
    float rad = 3.;
    vec3 off = vec3(rad*sin(t), 0., rad*cos(t));
    off.y = .2*sin(amp*u_bounce +t);

    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vec4(RotateY(vPosition, t) + off, 1.);
}
'''

basic_frag_src = '''
#version 330

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.);
}
'''

class Scene06(object):
    def __init__(self, n):
        self.bounce = 0
        self.cubes = ShaderWithVariables()
        self.n = n

    def initGL(self):
        print("initGL")
        """Shaders must be initialized after a GL context is available."""
        if not glUseProgram:
            print 'Missing Shader Objects!'
            return
        self.cubes.initGL_program(basic_vert_src, basic_frag_src)

        self.cubes.vao = glGenVertexArrays(1)
        glBindVertexArray(self.cubes.vao)
        self._InitCubeAttributes()
        glBindVertexArray(0)

    def _InitCubeAttributes(self):
        cols = [
            0,0,0,
            1,0,0,
            1,1,0,
            0,1,0,
            0,0,1,
            1,0,1,
            1,1,1,
            0,1,1,
        ]
        
        verts = [
            0,0,0,
            1,0,0,
            1,1,0,
            0,1,0,
            0,0,1,
            1,0,1,
            1,1,1,
            0,1,1,
        ]

        faces = [
            0,3,2, 1,0,2,
            4,5,6, 7,4,6,
            1,2,6, 5,1,6,
            2,3,7, 6,2,7,
            3,0,4, 7,3,4,
            0,1,5, 4,0,5
        ]

        vertex_data = numpy.array(verts, numpy.float32)

        self.cubes.vbos['vPosition'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.cubes.vbos['vPosition'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*3*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW)
        # Note: the last parameter has to be None, 0 won't work!
        glVertexAttribPointer(self.cubes.attrs['vPosition'], 3, GL_FLOAT, GL_FALSE, 0, None)
        
        self.cubes.vbos['vColor'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.cubes.vbos['vColor'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*3*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW)
        glVertexAttribPointer(self.cubes.attrs['vColor'], 3, GL_FLOAT, GL_FALSE, 0, None)

        glEnableVertexAttribArray(self.cubes.attrs['vPosition'])
        glEnableVertexAttribArray(self.cubes.attrs['vColor'])

        quad_data = numpy.array(faces, numpy.uint32)
        
        self.cubes.vbos['elements'] = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.cubes.vbos['elements'])
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(faces)*3*sizeof(GLuint), quad_data, GL_STATIC_DRAW)

    def timestep(self, absTime, dt):
        self.bounce += dt * 2 * (180 / math.pi)

    def RenderForOneEye(self, mview, persp):
        """Pass in the matrices"""
        glEnable(GL_CULL_FACE)
        glUseProgram(self.cubes.program)

        mv_array = numpy.array(mview, numpy.float32)
        proj_array = numpy.array(persp, numpy.float32)
        glUniformMatrix4fv(self.cubes.unis['mvmtx'], 1, GL_FALSE, mv_array)
        glUniformMatrix4fv(self.cubes.unis['prmtx'], 1, GL_FALSE, proj_array)

        glUniform1i(self.cubes.unis['u_numCubes'], self.n)
        glUniform1f(self.cubes.unis['u_bounce'], self.bounce)

        glBindVertexArray(self.cubes.vao)

        glDrawElementsInstanced(GL_TRIANGLES, 6*3*2, GL_UNSIGNED_INT, None, self.n)

        glBindVertexArray(0)
        glUseProgram(0)
