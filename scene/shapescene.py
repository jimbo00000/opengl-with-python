# shapescene.py
# Fabricate the shape of a 3D arrow(pointer) with a coaxial cylinder and cone.

from OpenGL.GL import *
import numpy
import math
import util.matrixmath as mm
from util.shaderwithvariables import *

basic_vert_src = '''
#version 330

in vec3 vPosition;
in vec3 vColor;

out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vec4(vPosition, 1.);
}
'''

basic_frag_src = '''
#version 330

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(vfColor, 1.);
}
'''

class ShapeScene(object):
    def __init__(self, n):
        self.cubes = ShaderWithVariables()
        self.n = n
        self.subdiv = 64

    def initGL(self):
        """Shaders must be initialized after a GL context is available."""
        if not glUseProgram:
            print 'Missing Shader Objects!'
            return
        self.cubes.initGL_program(basic_vert_src, basic_frag_src)

        self.cubes.vao = glGenVertexArrays(1)
        glBindVertexArray(self.cubes.vao)
        self._InitArrowShapeAttributes()
        glBindVertexArray(0)

    def exitGL(self):
        del self.cubes

    def _InitArrowShapeAttributes(self):
        """Fabricate a mesh's attribute data."""
        n = self.subdiv

        def make_cone_mesh():
            """Returns the vertices, colors and face indices of a cone."""
            verts = []
            cols = []
            faces = []
            r = .2
            dh = .5
            apex = [0,0,1]
            subapex = [0,0,1-dh]
            for i in range(n):
                phase = float(i)/float(n)
                phase_ = float(i+1)/float(n)
                rot = 2 * math.pi * phase
                rot_ = 2 * math.pi * phase_
                x,y = r*math.sin(rot), r*math.cos(rot)
                x_,y_ = r*math.sin(rot_), r*math.cos(rot_)
                verts.extend([ apex, [x,y,1-dh], [x_,y_,1-dh] ])
                ab = mm.subtract(verts[-1], verts[-3])
                ac = mm.subtract(verts[-2], verts[-3])
                norm = mm.normalize(mm.cross(ab,ac))
                verts.extend([ subapex, [x,y,1-dh], [x_,y_,1-dh] ])
                cols.extend([norm] * 3)
                cols.extend([[0,0,-1]]*3)
            for i in range(n):
                faces.append([6*i  , 6*i+2, 6*i+1])
                faces.append([6*i+3, 6*i+4, 6*i+5])
            return verts, cols, faces

        def make_cylinder_mesh():
            """Returns the vertices, colors and face indices of a cone."""
            verts = []
            cols = []
            faces = []
            r = .075
            zlen = .8
            for i in range(n):
                phase = float(i)/float(n)
                rot = 2 * math.pi * phase
                x,y = r*math.sin(rot), r*math.cos(rot)
                verts.extend([ [x,y,0], [x,y,zlen], [x,y,0], [0,0,0] ])
                cols.extend([mm.normalize([x,y,0])]*2)
                cols.append([0,0,-1]*2)
            for i in range(n-1):
                faces.append([4*i  , 4*i+1, 4*i+4])
                faces.append([4*i+4, 4*i+1, 4*i+5])
                faces.append([4*i+3, 4*i+2, 4*i+6])
            faces.append([1,0,4*n-3])
            faces.append([4*n-3,0,4*n-4])
            faces.append([4*n-2,2,4*n-1])
            return verts, cols, faces

        verts, cols, faces = make_cone_mesh()
        # Append cylinder verts/faces onto cone lists
        b = len(verts)
        v2,c2,f2 = make_cylinder_mesh()
        verts.extend(v2)
        cols.extend(c2)

        # Shift faces' vertex indices up by b when combining arrays
        f2 = [[b+i for i in f] for f in f2]
        faces.extend(f2)

        # Extend each 3-vector into a 4-vector with w=1
        sphverts = [v+[1] for v in verts]

        # Flatten lists of lists
        cols = [val for sublist in cols for val in sublist]
        verts = [val for sublist in sphverts for val in sublist]
        faces = [val for sublist in faces for val in sublist]
        self.num_verts = len(verts)

        vertex_data = numpy.array(verts, numpy.float32)
        color_data = numpy.array(cols, numpy.float32)

        self.cubes.vbos['vPosition'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.cubes.vbos['vPosition'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW)
        # Note: the last parameter has to be None, 0 won't work!
        glVertexAttribPointer(self.cubes.attrs['vPosition'], 4, GL_FLOAT, GL_FALSE, 0, None)
        
        self.cubes.vbos['vColor'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.cubes.vbos['vColor'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*sizeof(GLfloat), color_data, GL_STATIC_DRAW)
        glVertexAttribPointer(self.cubes.attrs['vColor'], 3, GL_FLOAT, GL_FALSE, 0, None)

        glEnableVertexAttribArray(self.cubes.attrs['vPosition'])
        glEnableVertexAttribArray(self.cubes.attrs['vColor'])

        quad_data = numpy.array(faces, numpy.uint32)
        
        self.cubes.vbos['elements'] = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.cubes.vbos['elements'])
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(faces)*sizeof(GLuint), quad_data, GL_STATIC_DRAW)

    def timestep(self, absTime, dt):
        pass

    def drawSingleArrow(self, mview, persp):
        """Pass in the matrices and draw a single arrow shape."""
        glEnable(GL_CULL_FACE)
        mview = mm.glh_scale(mview, 4,4,4)
        glUseProgram(self.cubes.program)
        glUniformMatrix4fv(self.cubes.unis['mvmtx'], 1, GL_FALSE, numpy.array(mview, numpy.float32))
        glUniformMatrix4fv(self.cubes.unis['prmtx'], 1, GL_FALSE, numpy.array(persp, numpy.float32))

        glBindVertexArray(self.cubes.vao)
        x = self.subdiv
        glDrawElements(GL_TRIANGLES, x*3*5, GL_UNSIGNED_INT, None)
        glBindVertexArray(0)

        glUseProgram(0)

    def RenderForOneEye(self, mview, persp):
        self.drawSingleArrow(mview, persp)

    def keyPressed(self, k):
        print("Key press: ",k)
