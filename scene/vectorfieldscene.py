# vectorfieldscene.py
# Visualize a vector field in space with line segments.

from OpenGL.GL import *
import numpy
import math
import util.matrixmath as mm
from util.shaderwithvariables import *
import shapescene

# A simple spherical field
vectorfield_spherical = '''
vec3 vectorField(in vec3 pos)
{
    vec3 del = pos;
    float s = 1. - step(4, length(del));
    return del * .25 * s;
}
'''

# A simple spherical field
vectorfield_sinusoid = '''
vec3 vectorField(in vec3 pos)
{
    return vec3(1.,0.,0.) + vec3(0., sin(pos.x), cos(pos.x));
}
'''

# A more interesting helical/radial shape
vectorfield_helical = '''
vec3 vectorField(in vec3 pos)
{
    vec3 wc = pos;
    float f = 1.;
    vec2 planeCenter = vec2(2.) + 1.*vec2(cos(f*wc.z), sin(f*wc.z));
    vec3 dp = vec3(wc.xy-planeCenter, 0.);

    float mag = 1. - smoothstep(0.,3.,length(dp));
    vec3 pull = -dp;
    pull.z = 1.;
    return mag * pull;
}
'''

# Using inverse square law
vectorfield_multiple_spheres = '''
vec3 vectorField(in vec3 pos)
{
    vec3 sph1 = vec3(-.5, 0., 0.);
    vec3 sph2 = vec3( .5, 0., 0.);
    vec3 sph3 = vec3( .0, -.2, -.5);
    vec3 negsph = vec3(.0, .0, 0.);

    vec3 tot = vec3(0.);

    float d1 = length(pos-sph1);
    tot += 2./(d1*d1) * (pos-sph1);

    float d2 = length(pos-sph2);
    tot += 2./(d2*d2) * (pos-sph2);

    float d3 = length(pos-sph3);
    tot -= 2./(d3*d3) * (pos-sph3);

    return .2 * tot;
}
'''

vectorfield_srcs = [
    vectorfield_spherical,
    vectorfield_sinusoid,
    vectorfield_helical,
    vectorfield_multiple_spheres
]

#
# Draw lines oriented along field using geometry shader.
#
vectorpoints_vert = '''
#version 330

in vec4 vPosition;

void main()
{
    vec3 pos3 = vPosition.xyz;
    vec4 pos4 = vec4(pos3, 1.);
    gl_Position = pos4; // apply matrices in geom shader
}
'''

vectorpoints_geom = '''
#version 330

layout(points) in;
layout(line_strip, max_vertices = 2) out;

out vec3 gfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

vec3 vectorField(in vec3 pos);

void main() {
    vec4 pos = gl_in[0].gl_Position;
    vec3 off = vectorField(pos.xyz);

    gl_Position = prmtx * mvmtx * pos;
    gfColor = abs(off);
    EmitVertex();

    gl_Position = prmtx * mvmtx * (pos + vec4(.5 * off,0.));
    gfColor = abs(off);
    EmitVertex();

    EndPrimitive();
}
#line 100
'''

vectorpoints_frag = '''
#version 330

in vec3 gfColor;
out vec4 fragColor;

void main()
{
    fragColor = vec4(gfColor, 1.0);
}
'''

#
# Draw arrows oriented along field lines using instanced geometry.
#
arrows_vert_src = '''
#version 330

in vec3 vPosition;
in vec3 vColor;

out vec3 vfNormal;
out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

vec3 vectorField(in vec3 pos);

// http://www.neilmendoza.com/glsl-rotation-about-an-arbitrary-axis/
mat4 rotationMatrix(float angle, vec3 axis)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

// http://stackoverflow.com/questions/193918/what-is-the-easiest-way-to-align-the-z-axis-with-a-vector
mat4 rotateZtoAxis(in vec3 targetDir)
{
    vec3 z = vec3(0.,0.,1.);
    targetDir = normalize(targetDir);
    float rotAngle = acos(dot(targetDir, z));
    if (abs(rotAngle) > .001)
    {
        vec3 rotAxis = normalize(cross(targetDir, z));
        return rotationMatrix(rotAngle, rotAxis);
    }
    return mat4(1.);
}

void main()
{
    int xi = gl_InstanceID % 10;
    int yi = (gl_InstanceID / 10) % 10;
    int zi = (gl_InstanceID / 100) % 10;
    vec3 off = 8. * .1 * vec3(float(xi), float(yi), float(zi));
    off -= vec3(4.);
    vec3 field = vectorField(off);

    vec3 vert = .5 * vPosition;
    vert = (rotateZtoAxis(field) * vec4(vert,1.)).xyz;
    vert *= length(field);
    vert += off;
    
    gl_Position = prmtx * mvmtx * vec4(vert, 1.);
    vfNormal = mat3(mvmtx) * vColor;
    vfColor = field;
}
#line 100
'''

arrows_frag_src = '''
#version 330

in vec3 vfNormal;
in vec3 vfColor;
out vec4 fragColor;

float shininess = 125.;
void main()
{
    vec3 N = vfNormal;
    vec3 L = normalize(vec3(0., 1., .5)); // direction *to* light, up like the sun
    vec3 E = vec3(0.);

    vec3 spec = vec3(0.);
    float bright = max(dot(N,L), 0.);
    if (bright > 0.)
    {
        vec3 H = normalize(L + E);
        float specBr = max(dot(H,N), 0.);
        spec = vec3(1.) * pow(specBr, shininess);
    }

    fragColor = vec4(abs(vfColor) * bright + spec, 1.);
}
'''

class VectorFieldScene(object):
    """Visualize a vector field in space.
    Uses a geometry shader to expand a 3D field of point samples into colored lines.
    Also contains geometry for a pointer mesh, inited and drawn in the class ShapeScene.
    """
    def __init__(self, n):
        self.fieldlines = ShaderWithVariables()
        self.arrows = ShaderWithVariables()
        self.subdiv = n
        self.ss = shapescene.ShapeScene(n)

    def initGL(self):
        """Shaders must be initialized after a GL context is available."""
        if not glUseProgram:
            print 'Missing Shader Objects!'
            return
        self.switchToField(0)

        self.fieldlines.vao = glGenVertexArrays(1)
        glBindVertexArray(self.fieldlines.vao)
        self._InitSampleFieldAttributes()
        glBindVertexArray(0)
        self.ss.initGL()

        self.arrows.vao = glGenVertexArrays(1)
        glBindVertexArray(self.arrows.vao)
        self.ss._InitArrowShapeAttributes()
        glBindVertexArray(0)

    def exitGL(self):
        del self.fieldlines
        del self.arrows

    def _InitSampleFieldAttributes(self):
        """Fabricate a subdivided cube"""
        n = self.subdiv

        verts = []
        coords = numpy.linspace(-4,4,n)
        for z in coords:
            for y in coords:
                for x in coords:
                    verts.append([x,y,z])
        self.num_verts = len(verts)

        # Flatten lists of lists
        verts = [val for sublist in verts for val in sublist]

        vertex_data = numpy.array(verts, numpy.float32)

        self.fieldlines.vbos['vPosition'] = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.fieldlines.vbos['vPosition'])
        glBufferData(GL_ARRAY_BUFFER, len(verts)*sizeof(GLfloat), vertex_data, GL_STATIC_DRAW)
        # Note: the last parameter has to be None, 0 won't work!
        glVertexAttribPointer(self.fieldlines.attrs['vPosition'], 3, GL_FLOAT, GL_FALSE, 0, None)
        
        glEnableVertexAttribArray(self.fieldlines.attrs['vPosition'])

    def switchToField(self, x):
        """Compile a new set of shaders with the given vector field index."""
        vf_src = vectorfield_srcs[x]
        glDeleteProgram(self.fieldlines.program)
        self.fieldlines.initgeom_program(
            vectorpoints_vert,
            vectorpoints_geom + vf_src,
            vectorpoints_frag)
        glDeleteProgram(self.arrows.program)
        self.arrows.initGL_program(
            arrows_vert_src + vf_src,
            arrows_frag_src)

    def timestep(self, absTime, dt):
        self.ss.timestep(absTime, dt) # Nothing happens in here, but seems correct to give it a chance.

    def drawFieldAsLines(self, mview, persp):
        """Use a geometry shader to expand GL_POINTS geometry into GL_LINES pairs."""
        fld = self.fieldlines
        if fld.program == 0:
            return
        glUseProgram(fld.program)

        mv_array = numpy.array(mview, numpy.float32)
        pr_array = numpy.array(persp, numpy.float32)
        glUniformMatrix4fv(fld.unis['mvmtx'], 1, GL_FALSE, mv_array)
        glUniformMatrix4fv(fld.unis['prmtx'], 1, GL_FALSE, pr_array)

        glBindVertexArray(fld.vao)
        glDrawArrays(GL_POINTS, 0, self.num_verts)
        glBindVertexArray(0)
        
        glUseProgram(0)

    def drawArrowField(self, mview, persp):
        """Draw a field of instanced geometry indicating field strength."""
        swv = self.arrows
        if swv.program == 0:
            return
        glUseProgram(swv.program)
        glUniformMatrix4fv(swv.unis['mvmtx'], 1, GL_FALSE, numpy.array(mview, numpy.float32))
        glUniformMatrix4fv(swv.unis['prmtx'], 1, GL_FALSE, numpy.array(persp, numpy.float32))

        glBindVertexArray(swv.vao)
        x = self.ss.subdiv
        glDrawElementsInstanced(GL_TRIANGLES, x*3*5, GL_UNSIGNED_INT, None, 10*10*10)
        glBindVertexArray(0)

        glUseProgram(0)

    def RenderForOneEye(self, mview, persp):
        """Pass in the matrices"""
        self.drawFieldAsLines(mview, persp)

        self.drawArrowField(mview, persp)
        #self.ss.RenderForOneEye(mview, persp)
