# snake.py
# Simply include two components

from OpenGL.GL import *
import numpy
import math
import util.matrixmath as mm
from util.shaderwithvariables import *
import snakebodyscene
import snakeheadscene

class SnakeScene(object):
    """Draw the body and head in the same space.
    """
    def __init__(self, n):
        self.body = snakebodyscene.SnakeBodyScene(n)
        self.head = snakeheadscene.SnakeHeadScene(n)

    def initGL(self):
        self.body.initGL()
        self.head.initGL()

    def exitGL(self):
        self.body.exitGL()
        self.head.exitGL()

    def runComputeShader(self, strength):
        # body has no compute shader
        self.head.runComputeShader(strength)

    def timestep(self, absTime, dt):
        self.body.timestep(absTime, dt)
        self.head.timestep(absTime, dt)

    def RenderForOneEye(self, mview, persp):
        """Pass in the matrices"""
        self.body.RenderForOneEye(mview, persp)
        mvr = mm.glh_rotate(mview, -15, [1,0,0])
        self.head.RenderForOneEye(mvr, persp)
