# coding=utf-8

import pyglfw.pyglfw as fw
from pyglfw.libapi import *
from ctypes import *

import OpenGL
OpenGL.ERROR_CHECKING = False
from OpenGL.GL import *
import numpy
import util.matrixmath as mm
import util.fps
import time
import sys
import getopt
import platform
import traceback

# Scenes
import scene.vectorfieldscene as vectorfieldscene
import scene.shapescene as shapescene
import scene.snakeheadscene as snakeheadscene
import scene.fragshaderscene as fragshaderscene
import scene.texquadscene as texquadscene
import scene.voronoifragshaderscene as voronoifragshaderscene
import scene.snake as snake

# Effects
import effect.onepasseffect as onepasseffect
import effect.edgehighlighteffect as edgehighlighteffect
import effect.chromaticeffect as chromaticeffect

win_w, win_h = 600, 400
clickpos = (0,0)
holding = [False, False, False]
objrot = (0,0)
objz = -10
campan = (0,0)

s = None
e = None

class CbWindow(fw.Window):
    def __init__(self, *args, **kwargs):
        super(CbWindow, self).__init__(*args, **kwargs)

        self.set_key_callback(CbWindow.key_callback)
        self.set_char_callback(CbWindow.char_callback)
        self.set_scroll_callback(CbWindow.scroll_callback)
        self.set_mouse_button_callback(CbWindow.mouse_button_callback)
        self.set_cursor_enter_callback(CbWindow.cursor_enter_callback)
        self.set_cursor_pos_callback(CbWindow.cursor_pos_callback)
        self.set_window_size_callback(CbWindow.window_size_callback)
        self.set_window_pos_callback(CbWindow.window_pos_callback)
        self.set_window_close_callback(CbWindow.window_close_callback)
        self.set_window_refresh_callback(CbWindow.window_refresh_callback)
        self.set_window_focus_callback(CbWindow.window_focus_callback)
        self.set_window_iconify_callback(CbWindow.window_iconify_callback)
        self.set_framebuffer_size_callback(CbWindow.framebuffer_size_callback)

    def key_callback(self, key, scancode, action, mods):
        if action == GLFW_PRESS or action == GLFW_REPEAT:
            global s, e
            print(mods)

            #if s.keyPressed != None: #Todo how to tell if function exists?
            # Todo: check return values
            try:
                s.keyPressed(key)
            except:
                pass
            try:
                e.keyPressed(key)
            except:
                pass


            # Select Effects with control + F keys
            if mods == GLFW_MOD_CONTROL:
                if key == GLFW_KEY_F1:
                    if e != None:
                        e.exitGL()
                    e = None
                elif key == GLFW_KEY_F2:
                    if e != None:
                        e.exitGL()
                    e = onepasseffect.OnePassEffect(win_w, win_h)
                    initGL(e)
                elif key == GLFW_KEY_F3:
                    if e != None:
                        e.exitGL()
                    e = edgehighlighteffect.EdgeHighlightEffect(win_w, win_h)
                    initGL(e)
                elif key == GLFW_KEY_F4:
                    if e != None:
                        e.exitGL()
                    e = chromaticeffect.ChromaticEffect(win_w, win_h)
                    initGL(e)
            else:
                # Todo: would this be better handled with an optional function in Scene?
                strength = .1
                if mods == GLFW_MOD_SHIFT:
                    strength *= -1
                keyfuncs = {
                    GLFW_KEY_ENTER: lambda x: s.runComputeShader(strength),
                }

                if key in keyfuncs:
                    keyfuncs[key](None)
                elif GLFW_KEY_1 <= key <= GLFW_KEY_9:
                    s.switchToField(key-GLFW_KEY_1)
                # Select scene
                # Is it possible to make a func table out of this part?
                elif key == GLFW_KEY_F1:
                    s.exitGL()
                    s = vectorfieldscene.VectorFieldScene(12)
                    initGL(s)
                elif key == GLFW_KEY_F2:
                    s.exitGL()
                    s = snake.SnakeScene(12)
                    initGL(s)
                elif key == GLFW_KEY_F3:
                    s.exitGL()
                    s = shapescene.ShapeScene(12)
                    initGL(s)
                elif key == GLFW_KEY_F5:
                    s.exitGL()
                    s = fragshaderscene.FragShaderScene()
                    initGL(s)
                elif key == GLFW_KEY_F6:
                    s.exitGL()
                    s = voronoifragshaderscene.VoronoiFragShaderScene()
                    initGL(s)
                elif key == GLFW_KEY_F7:
                    s.exitGL()
                    s = texquadscene.TexQuadScene()
                    initGL(s)

    def char_callback(self, char):
        print("unichr: char=%s" % char)

    def scroll_callback(self, off_x, off_y):
        global objz
        objz += off_y
        objz = numpy.clip(objz, -20, -1)

    def mouse_button_callback(self, button, action, mods):
        print("button: button=%s action=%s mods=%s" % (button, action, mods))
        global holding
        global clickpos
        if action == GLFW_PRESS:
            holding[button] = True
            clickpos = self.cursor_pos

        elif action == GLFW_RELEASE:
            holding[button] = False

    def cursor_enter_callback(self, status):
        print("cursor: status=%s" % status)

    def cursor_pos_callback(self, pos_x, pos_y):
        global holding
        global clickpos
        global objrot
        global campan
        if holding[0]:
            objrot = (pos_x-clickpos[0], pos_y-clickpos[1])
        elif holding[2]:
            campan = (.01*(pos_x-clickpos[0]), -.01*(pos_y-clickpos[1]))

    def window_size_callback(self, wsz_w, wsz_h):
        print("window: w=%s h=%s" % (wsz_w, wsz_h))
        global win_w, win_h
        win_w, win_h = wsz_w, wsz_h
        if e != None:
            e.ResizeFBO(wsz_w, wsz_h)

    def window_pos_callback(self, pos_x, pos_y):
        print("window: x=%s y=%s" % (pos_x, pos_y))

    def window_close_callback(self):
        print("should: %s" % self.should_close)

    def window_refresh_callback(self):
        print("redraw")

    def window_focus_callback(self, status):
        print("active: status=%s" % status)

    def window_iconify_callback(self, status):
        print("hidden: status=%s" % status)

    def framebuffer_size_callback(self, fbs_x, fbs_y):
        print("buffer: x=%s y=%s" % (fbs_x, fbs_y))


change_markers = {fw.Monitor.CONNECTED: '+', fw.Monitor.DISCONNECTED: '-'}


def on_monitor(_monitor, _event):
    change = change_markers.get(_event, '~')
    print("screen: %s %s" % (change, _monitor.name))


def initGL(s):
    s.initGL()
    if e != None:
        e.initGL()


def display(s):
    global win_w,win_h
    glViewport(0,0,win_w,win_h)
    glClearColor(0.2, 0.2, 0.2, 0.0)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glEnable(GL_DEPTH_TEST)

    camera_location = [0.0, 2.0, 3.0]
    look_vector = [0.0, -0.3, -1.0]
    origin = list(camera_location)  # deep copy
    origin[0] += look_vector[0]
    origin[1] += look_vector[1]
    origin[2] += look_vector[2]
    up_vector = [0.0, 1.0, 0.0]
    mvmtx = mm.make_identity_matrix()
    global objrot, objz, campan
    mvmtx = mm.glh_look_at(mvmtx, camera_location, origin, up_vector)
    mvmtx = mm.glh_translate(mvmtx, campan[0], campan[1], objz)
    mvmtx = mm.glh_rotate(mvmtx, objrot[1], [1,0,0])
    mvmtx = mm.glh_rotate(mvmtx, objrot[0], [0,1,0])

    aspect_ratio = float(win_w) / float(win_h)
    prmtx = mm.perspective_rh(45.0, aspect_ratio, 0.004, 500.0)

    s.RenderForOneEye(mvmtx, prmtx)


def displayWithEffect(s, e):
    if e != None:
        e.BindFBO()
    display(s)
    if e != None:
        e.UnbindFBO()
        glDisable(GL_DEPTH_TEST)
        glViewport(0, 0, win_w, win_h)
        e.Present(win_w, win_h)


def timestep(s, absTime, dt):
    s.timestep(absTime, dt)
    if e != None:
        e.timestep(absTime, dt)


DEBUGCB = WINFUNCTYPE(c_void_p, c_int, c_int, c_int, c_int, c_int, POINTER(c_char), c_void_p) if platform.system() == 'Windows' else CFUNCTYPE(c_void_p, c_int, c_int, c_int, c_int, c_int, POINTER(c_char), c_void_p)

def myCallback(source, type, id, severity, length, msg, data):
    enum_table = {
        GL_DEBUG_SEVERITY_HIGH : "SEVERITY_HIGH",
        GL_DEBUG_SEVERITY_MEDIUM : "SEVERITY_MEDIUM",
        GL_DEBUG_SEVERITY_LOW : "SEVERITY_LOW",
        GL_DEBUG_SEVERITY_NOTIFICATION : "SEVERITY_NOTIFICATION",
        GL_DEBUG_SOURCE_API : "SOURCE_API",
        GL_DEBUG_SOURCE_WINDOW_SYSTEM : "SOURCE_WINDOW_SYSTEM",
        GL_DEBUG_SOURCE_SHADER_COMPILER : "SOURCE_SHADER_COMPILER",
        GL_DEBUG_SOURCE_THIRD_PARTY : "SOURCE_THIRD_PARTY",
        GL_DEBUG_SOURCE_APPLICATION : "SOURCE_APPLICATION",
        GL_DEBUG_SOURCE_OTHER : "SOURCE_OTHER",
        GL_DEBUG_TYPE_ERROR : "TYPE_ERROR",
        GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR : "TYPE_DEPRECATED_BEHAVIOR",
        GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR : "TYPE_UNDEFINED_BEHAVIOR",
        GL_DEBUG_TYPE_PORTABILITY : "TYPE_PORTABILITY",
        GL_DEBUG_TYPE_PERFORMANCE : "TYPE_PERFORMANCE",
        GL_DEBUG_TYPE_MARKER : "TYPE_MARKER",
        GL_DEBUG_TYPE_PUSH_GROUP : "TYPE_PUSH_GROUP",
        GL_DEBUG_TYPE_POP_GROUP : "TYPE_POP_GROUP",
        GL_DEBUG_TYPE_OTHER : "TYPE_OTHER",
    }
    print(enum_table[source], enum_table[type], enum_table[severity], id)
    print("   "+msg)
    traceback.print_stack()
    #print("".join(traceback.format_stack()))
    exit()


def main(argv):
    n = 12
    scenetype = -1
    try:
        opts, args = getopt.getopt(argv,"n:s:")
    except getopt.GetoptError:
        print 'test.py -n <# of cubes> -s <Scene #>'
        sys.exit(2)

    for opt, arg in opts:
        if opt == "-n":
            n = int(arg)
        elif opt == "-s":
            scenetype = int(arg)

    try:
        fw.init()
    except fw.errors.PlatformError as ex:
        print(ex)

    fw.Monitor.set_callback(on_monitor)
    fw.Window.hint(depth_bits=16)
    fw.Window.hint(context_version_major=4)
    fw.Window.hint(context_version_minor=1)
    fw.Window.hint(opengl_forward_compat=1)
    #fw.Window.hint(opengl_profile=GLFW_OPENGL_CORE_PROFILE)
    fw.Window.hint(debug_context=GL_TRUE)
    win = CbWindow(win_w, win_h, "callback window")
    win.make_current()
    glfwSwapInterval(1)

    print(glGetString(GL_VENDOR))
    print(glGetString(GL_RENDERER))
    print(glGetString(GL_VERSION))
    print(glGetString(GL_SHADING_LANGUAGE_VERSION))

    cbfunc = GLDEBUGPROC(myCallback)
    glDebugMessageCallback(cbfunc, None)
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, None, GL_TRUE)
    #glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER, 0,
    #    GL_DEBUG_SEVERITY_NOTIFICATION, -1 , "Start debugging")
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS)

    global s
    if scenetype == 1:
        import scene.scene01 as scene
        s = scene.Scene01(n)
        print("Scene01")
    elif scenetype == 2:
        import scene.scene02 as scene
        s = scene.Scene02(n)
        print("Scene02")
    elif scenetype == 3:
        import scene.scene03 as scene
        s = scene.Scene03(n)
        print("Scene03")
    elif scenetype == 4:
        import scene.scene04 as scene
        s = scene.Scene04(n)
        print("Scene04")
    elif scenetype == 5:
        import scene.scene05 as scene
        s = scene.Scene05(n)
        print("Scene05")
    elif scenetype == 6:
        import scene.scene06 as scene
        s = scene.Scene06(n)
        print("Scene06")
    elif True:
        import scene.shapescene as shapescene
        s = shapescene.ShapeScene(12)
    elif True:
        import scene.snake as scene
        s = scene.SnakeScene(n)
    else:
        import scene.tubescene as scene
        s = scene.TubeScene(n)

    initGL(s)
    fpsctr = util.fps.FPSTimer()
    last_frame_time = time.clock()
    while not win.should_close:
        displayWithEffect(s, e)

        now = time.clock()
        timestep(s, now, now - last_frame_time)
        last_frame_time = now
        fpsctr.on_frame()

        win.swap_buffers()
        fw.poll_events()

        # On Linux, this may be VERY slow
        title = "".join(["PyOpenGL: ", str(fpsctr.get_fps()), " fps"])
        win.set_title(title)

        if win.keys.escape:
            win.should_close = True

    del s
    fw.terminate()

if __name__ == '__main__':
    main(sys.argv[1:])
