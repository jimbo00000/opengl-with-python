# coding=utf-8

import pyglfw.pyglfw as fw
from pyglfw.libapi import *
from ctypes import *

import OpenGL
OpenGL.ERROR_CHECKING = False
from OpenGL.GL import *

import sys

win_w, win_h = 800, 600

class CbWindow(fw.Window):
    def __init__(self, *args, **kwargs):
        super(CbWindow, self).__init__(*args, **kwargs)
        self.set_window_size_callback(CbWindow.window_size_callback)

    def window_size_callback(self, wsz_w, wsz_h):
        print("window: w=%s h=%s" % (wsz_w, wsz_h))
        global win_w, win_h
        win_w, win_h = wsz_w, wsz_h

change_markers = {fw.Monitor.CONNECTED: '+', fw.Monitor.DISCONNECTED: '-'}

def on_monitor(_monitor, _event):
    change = change_markers.get(_event, '~')
    print("screen: %s %s" % (change, _monitor.name))

def main(argv):
    try:
        fw.init()
    except fw.errors.PlatformError as e:
        print(e)

    fw.Monitor.set_callback(on_monitor)
    win = CbWindow(win_w, win_h, "callback window")
    win.make_current()

    print('GL_VENDOR=' + glGetString(GL_VENDOR))
    print('GL_RENDERER=' + glGetString(GL_RENDERER))
    print('GL_VERSION=' + glGetString(GL_VERSION))

    while not win.should_close:
        glViewport(0, 0, win_w, win_h)
        glClearColor(0.5, 0.9, 0.5, 0.0)
        glClear(GL_COLOR_BUFFER_BIT)

        win.swap_buffers()
        fw.poll_events()
        if win.keys.escape:
            win.should_close = True


if __name__ == '__main__':
    main(sys.argv[1:])
