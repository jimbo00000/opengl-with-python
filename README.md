# OpenGL with Python #

## Description ##
A simple testbed to get started in OpenGL programming using python.

### Setup ###

#### Windows 8 ####
 - Install Python 2.7 or 3.4
   - Be sure to choose the 32 bit version (64 bit doesn't link to glfw correctly on Windows)
 - `C:\Python27\Scripts\pip.exe install PyOpenGL PyOpenGL_accelerate`
 - `C:\Python27\Scripts\pip.exe install pyglfw`
   - These steps may throw exceptions while succeeding. Just try again until it works.
 - Install numpy from: [sourceforge.net/projects/numpy/](http://sourceforge.net/projects/numpy/files/NumPy/1.9.2/)
 - Install [graphviz](http://www.graphviz.org/Download_windows.php)
 - Add python's install directory to $PATH (This PC->Properties->Advanced System Settings->Environment Variables) `;C:\Python27`
 - Add dot's install directory to $PATH `;C:\Program Files (x86)\Graphviz2.38\bin`

#### Fedora 21 ####
 - add /usr/local/lib to /etc/ld.so.conf (is this necessary?)
 - `sudo yum install python-pip python-devel glfw graphviz`
 - `sudo pip install PyOpenGL PyOpenGL_accelerate pyglfw numpy`
 - `sudo ldconfig`

#### Ubuntu 15 ####
 - `sudo apt-get install mesa-common-dev mesa-utils-extra libgl1-mesa-dev libglapi-mesa mesa-utils`
 - `sudo apt-get install libxrandr-dev libxinerama-dev libxcursor-dev python-dev python-pip`
 - `sudo pip install PyOpenGL PyOpenGL_accelerate pyglfw numpy`
 - `sudo ldconfig`
 - Build glfw from source:
 - `sudo apt-get install cmake cmake-gui`
 - `cd glfw...; cmake-gui .; cdbuild; make; sudo make install`

#### PCBSD ####
 - `sudo pkg install -fy python27-2.7.10 glfw`
 - `sudo pip install PyOpenGL PyOpenGL_accelerate pyglfw numpy`
 - `LD_PRELOAD=/lib/libthr.so.3 python stats/getstats.py`

#### Solaris 11 ####
 - `sudo pkg install developer/versioning/git`
 - `sudo pkg install gcc graphviz`
 - `sudo pkgadd -d http://get.opencsw.org/now`
 - `sudo /opt/csw/bin/pkgutil -U`
 - `sudo /opt/csw/bin/pkgutil -y -i cmake`
 - Build and install glfw with cmake 3.3.1 from `/opt/csw/bin/cmake`:  
   - `/opt/csw/bin/cmake -DBUILD_SHARED_LIBS=ON`
   - `make`
   - `sudo make install`
 - `LD_LIBRARY_PATH=/usr/local/lib python main_glfw.py`


### Usage ###
Just double-click main_glfw.py.
Or, launch python with it as the first parameter:

    python main_glfw.py

To generate a graph of function hotspots:

    python stats/getstats.py

