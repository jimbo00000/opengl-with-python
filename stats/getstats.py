#! /usr/bin/env python
'''
getstats.py
Try to run this script as unobtrusively as possible:
Just inject its name after 'python' in a command line and have it dump
all intermediate output into the stats directory where this script resides.
'''

import os
import sys

def run_command(cmd):
    print cmd
    os.system(cmd)

exec_file = "moderngl_glfw.py"
exec_args = sys.argv
if len(sys.argv) >= 2:
    exec_args = sys.argv[1:]

# argv[0] should be the name of this script
stats_dir = os.path.dirname(sys.argv[0])
stats_file = os.path.join(stats_dir, "stats")

cmd = "{0} -m cProfile -o {1} {2} {3}".format(sys.executable, stats_file, exec_file, ' '.join(exec_args))
run_command(cmd)

graph_exe = os.path.join(stats_dir, "gprof2dot.py")
stats_png = ''.join([stats_file, ".png"])
cmd = "{0} {1} -f pstats {2} | dot -Tpng -o {3}".format(sys.executable, graph_exe, stats_file, stats_png)
run_command(cmd)


import platform
# Show the image
if "Windows" in platform.platform():
    run_command(stats_png)
elif "Linux" in platform.platform():
    run_command("eog " + stats_png)
